#include<stdio.h>

struct student_details
{
    char name[50];
    char sub[50];
    int num;
};

int main()
{
    struct student_details std[5];
    int i;
    printf("\n");
    for(i=0; i<5; i++)
    {
        printf("Enter student no.%d's name : ",i+1);
        scanf("%s",&std[i].name);
        printf("Enter student no.%d's subject : ",i+1);
        scanf("%s",&std[i].sub);
        printf("Enter student no.%d's marks : ",i+1);
        scanf("%d",&std[i].num);
        printf("\n");
    }
    printf("\n");
    printf("Details of students are shown below\n");
    printf("\n");
    for(i=0; i<5; i++)
    {
        printf("Student no.%d's name is : %s \n",i+1,std[i].name);
        printf("Student no.%d's subject : %s \n",i+1,std[i].sub);
        printf("Student no.%d's marks : %d\n",i+1,std[i].num);
        printf("\n");
    }
    return 0;
}
